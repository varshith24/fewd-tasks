import React, { useState } from 'react'
function TableRow(props) {
    const e = props.details
    const [flag, setFlag] = useState(true)
    const handleColor = () => {
        // console.log(e)
        const myelement = document.getElementById(e.id)
        const myName = document.getElementById(e.name)
        if (flag) {
            myelement.style.backgroundColor = "slategray"
            myName.style.textTransform = "uppercase"
        }
        else {
            myelement.style.backgroundColor = "white"
            myName.style.textTransform = "captalize"
            console.log(myName.value)
        }
        setFlag(() => !flag)
    }
    return (
        <tr key={e.id} id={e.id} onClick={() => handleColor()} >
            <td>{e.id}</td>
            <td id={e.name} >{e.name}</td>
            <td>{e.ticketnumber}</td>
            <td>{e.ratinggrade}</td>
            <td>{e.examgrade}</td>
            <td>{e.finalgrade}</td>
            <td style={{ backgroundColor: e.finalgrade > 4 ? 'green' : "red" }} >{e.finalgrade > 4 ? "Passed" : "Failed"}</td>
            <td><button>Details</button></td>
        </tr>
    )
}
export default TableRow