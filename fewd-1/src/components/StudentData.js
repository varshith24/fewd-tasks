import React from 'react'
// import studata from '../json/studata.json'
import TableRow from './TableRow'


function StudentData(props) {
    return (
        <table border="2" align='center' >
            <thead>
                <tr>
                    <th>
                        Id
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Ticket Number
                    </th>
                    <th>
                        ratinggrade
                    </th>
                    <th>
                        examgrade
                    </th>
                    <th>
                        Final Grade
                    </th>
                    <th>Status</th>
                    <th>Details</th>
                </tr>
            </thead>
            <tbody>
                {
                    props.data.map((e, index) => (
                        <TableRow key={e.id.toString()} details={e} index={index} />
                    ))
                }
            </tbody>
        </table>
    )
}

export default StudentData