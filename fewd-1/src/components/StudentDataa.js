const data = [
    {
        id: '1', name: 'pavan', ticketnumber: '100', tickettopic: 'supply', examgrade: 8.0, ratinggrade: 6.0,
        comments: 'pass'
    },
    {
        id: '16', name: 'gopi', ticketnumber: '200', tickettopic: 'regular', examgrade: 4.0, ratinggrade: 6.0,
        comments: 'pass'
    },
    {
        id: '2', name: 'vivek', ticketnumber: '300', tickettopic: 'advanced', examgrade: 9.0, ratinggrade: 5.0,
        comments: 'pass'
    },
    {
        id: '3', name: 'sateesh', ticketnumber: '400', tickettopic: 'summerterm', examgrade: 3.0, ratinggrade: 1.0,
        comments: 'fail'
    },
    {
        id: '4', name: 'anusha', ticketnumber: '500', tickettopic: 'supricetest', examgrade: 4.5, ratinggrade: 3.0,
        comments: 'pass'
    },
    {
        id: '5', name: 'ankitha', ticketnumber: '600', tickettopic: 'additionalcourse', examgrade: 8.0, ratinggrade: 7.0,
        comments: 'pass'
    },
    {
        id: '6', name: 'Raju', ticketnumber: '700', tickettopic: 'mswd', examgrade: 6.0, ratinggrade: 5.0,
        comments: 'pass'
    },
    {
        id: '7', name: 'anitha', ticketnumber: '800', tickettopic: 'additionalcourse', examgrade: 8.0, ratinggrade: 7.0,
        comments: 'pass'
    },
    {
        id: '8', name: 'Satheesh', ticketnumber: '900', tickettopic: 'javascripttest', examgrade: 9.0, ratinggrade: 8.0,
        comments: 'pass'
    },
    {
        id: '9', name: 'Rambabu', ticketnumber: '910', tickettopic: 'htmltest', examgrade: 7.0, ratinggrade: 6.0,
        comments: 'pass'
    },
    {
        id: '10', name: 'Bunny varma', ticketnumber: '920', tickettopic: 'csstest', examgrade: 4.0, ratinggrade: 4.0,
        comments: 'fail'
    },
    {
        id: '11', name: 'Sameer', ticketnumber: '930', tickettopic: 'react-jstest', examgrade: 8.0, ratinggrade: 7.0,
        comments: 'pass'
    },
    {
        id: '12', name: 'Abhinav', ticketnumber: '940', tickettopic: 'angular-jstest', examgrade: 9.0, ratinggrade: 8.0,
        comments: 'pass'
    },
    {
        id: '13', name: 'Ashwin devashu', ticketnumber: '950', tickettopic: 'jsontest', examgrade: 8.0, ratinggrade: 7.0,
        comments: 'pass'
    },
    {
        id: '14', name: 'Abhijith', ticketnumber: '960', tickettopic: 'Ajaxtest', examgrade: 7.0, ratinggrade: 6.0,
        comments: 'pass'
    },
    {
        id: '15', name: 'Rohith', ticketnumber: '970', tickettopic: 'MongoDBtest', examgrade: 3.0, ratinggrade: 3.0,
        comments: 'fail'
    }
];


function getStatus(grade) {
    return (grade > 4) ? 'passed' : 'failed';
}
data.forEach((a) =>
(a.finalgrade = (a.ratinggrade * 0.4 + a.examgrade * 0.6).toPrecision(2),
    (a.status = getStatus(a.finalgrade))))
export default data;