// import logo from './logo.svg';
import { useEffect, useState } from 'react';
import './App.css';
import StudentData from './components/StudentData';
import data1 from './components/StudentDataa'
function App() {
  const [data, setData] = useState(data1)
  const [nametoogle, setNameToogle] = useState(true)
  const [finaltoogle, setFinalToogle] = useState(true)
  const [inputValue, setInputValue] = useState('')
  const handleAll = () => {
    setData(() => data1)
  }
  const handleFilter = (key, value) => {
    setData(data1.filter((obj) => {
      return obj[key] === value;
    }))
  }

  const handleInputChange = (e) => {
    setInputValue(e)
  }
  useEffect(() => {
    setData(data1.filter((obj) => {
      return obj.name.toLowerCase().includes(inputValue);
    }))
  }, [inputValue])

  function handleSortByName() {

    if (nametoogle) {
      setData(data1.sort((a, b) => {
        let fa = a.name.toLowerCase();
        let fb = b.name.toLowerCase();
        return fa.localeCompare(fb)
      }))
    }
    else {
      setData(data1.sort((a, b) => {
        let fa = a.name.toLowerCase();
        let fb = b.name.toLowerCase();
        return fb.localeCompare(fa)
      }))
    }
    setNameToogle(() => !nametoogle)
  }
  function handleSortByFinal() {
    if (finaltoogle) {
      setData(data1.sort((a, b) => {
        return a.finalgrade - b.finalgrade
      }))
    }
    else {
      setData(data1.sort((a, b) => {
        return b.finalgrade - a.finalgrade
      }))
    }
    setFinalToogle(() => !finaltoogle)
  }
  return (
    <div className="App">
      <button onClick={() => handleAll()}>All</button>
      <button onClick={() => handleFilter('status', 'passed')}>passed</button>
      <button onClick={() => handleFilter('status', 'failed')}>Failed</button>
      <button onClick={() => handleSortByName()} >A-Z/Z-A</button>
      <button onClick={() => handleSortByFinal()} >1-10/10-1</button>
      <input placeholder='Search by name' onChange={(e) => {
        // console.log(e);
        handleInputChange(e.target.value)
      }} />
      <StudentData data={data} />
    </div>
  );
}

export default App;
