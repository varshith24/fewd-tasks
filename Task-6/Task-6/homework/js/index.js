function visitLink(path) {
	//your code goes here
	if (localStorage.getItem(path) === null) {
		localStorage.setItem(path, 1)
	}
	else {
		localStorage.setItem(path, parseInt(localStorage.getItem(path)) + 1)
	}
}

function viewResults() {
	//your code goes here
	const page1 = localStorage.getItem('Page1')
	const page2 = localStorage.getItem('Page2')
	const page3 = localStorage.getItem('Page3')

	// console.log(page1 + " " + page2 + " " + page3)

	str = '<ul><li>You Visited Page 1 ' + page1 + ' time(s)</li>';
	str += '<li>You Visited Page 2 ' + page2 + ' time(s)</li>';
	str += '<li>You Visited Page 3 ' + page3 + ' time(s)</li></ul>';

	const content = document.getElementById('content')
	content.innerHTML += str;

	localStorage.clear()
}
